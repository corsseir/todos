import React, { useState, useEffect } from 'react';

import TodosHeader from '../../molecules/TodosHeader/TodosHeader';
import TodosBody from '../../molecules/TodosBody/TodosBody';
import TodosFooter from '../../molecules/TodosFooter/TodosFooter';
import { TodosItemModel } from '../../../models/TodosItemModel';

import axios, { AxiosResponse } from 'axios';

const TodosList: React.FC = () => {
  const [isTodosFormVisible, setIsTodosFormVisible] = useState(false);
  const [todosList, setTodosList] = useState(new Array<TodosItemModel>());

  useEffect(() => {
    axios.get('http://localhost:8000/api/tasks').then((res: AxiosResponse) => {
      const todosList = new Array<TodosItemModel>();

      res.data.forEach((e1: any) => {
        todosList.push({
          id: e1.id,
          text: e1.text,
          isDone: e1.is_done
        });
      });
      setTodosList(todosList);
    });
  }, []);

  return (
    <div className="row">
      <div className="col-12">
        <TodosHeader />
      </div>
      <div className="col-12">
        <TodosBody
          isTodosFormVisible={isTodosFormVisible}
          setIsTodosFormVisible={setIsTodosFormVisible}
          todosList={todosList}
          setTodosList={setTodosList}
        />
      </div>
      {!isTodosFormVisible ? (
        <div className="col-12">
          <TodosFooter
            isTodosFormVisible={isTodosFormVisible}
            setIsTodosFormVisible={setIsTodosFormVisible}
          />
        </div>
      ) : null}
    </div>
  );
};

export default TodosList;
