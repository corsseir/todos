import './TodosHeaderMonth.css';

import React from 'react';
import { TodosHeaderDatetimePropsModel } from '../../../models/TodosHeaderDatetimePropsModel';

const TodosHeaderMonth: React.FC<TodosHeaderDatetimePropsModel> = (
  props: TodosHeaderDatetimePropsModel
) => {
  return (
    <span className="font-weight-bold text-uppercase TodosHeaderMonth-font-size">
      {props.datetime.toFormat('LLL')}
    </span>
  );
};

export default TodosHeaderMonth;
