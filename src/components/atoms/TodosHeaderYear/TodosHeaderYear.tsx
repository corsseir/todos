import './TodosHeaderYear.css';

import React from 'react';
import { TodosHeaderDatetimePropsModel } from '../../../models/TodosHeaderDatetimePropsModel';

const TodosHeaderYear: React.FC<TodosHeaderDatetimePropsModel> = (
  props: TodosHeaderDatetimePropsModel
) => {
  return (
    <span className="font-weith-bold mt-1 TodosHeaderYear-font-size">
      {props.datetime.toFormat('yyyy')}
    </span>
  );
};

export default TodosHeaderYear;
