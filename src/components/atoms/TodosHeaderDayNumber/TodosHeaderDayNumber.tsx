import './TodosHeaderDayNumber.css';

import React from 'react';

import { TodosHeaderDatetimePropsModel } from '../../../models/TodosHeaderDatetimePropsModel';

const TodosHeaderDayNumber: React.FC<TodosHeaderDatetimePropsModel> = (
  props: TodosHeaderDatetimePropsModel
) => {
  return (
    <span className="font-weight-bold mr-1 TodosHeaderDayNumber-font-size">
      {props.datetime.toFormat('dd')}
    </span>
  );
};

export default TodosHeaderDayNumber;
