import './TodosHeaderDayName.css';

import React from 'react';
import { TodosHeaderDatetimePropsModel } from '../../../models/TodosHeaderDatetimePropsModel';

const TodosHeaderDayName: React.FC<TodosHeaderDatetimePropsModel> = (
  props: TodosHeaderDatetimePropsModel
) => {
  return (
    <span className="font-weight-bold text-uppercase TodosHeaderDayName-font-size">
      {props.datetime.toFormat('cccc')}
    </span>
  );
};

export default TodosHeaderDayName;
