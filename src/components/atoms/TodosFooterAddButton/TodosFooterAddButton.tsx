import './TodosFooterAddButton.css';

import React, { Dispatch, MouseEvent, ReactNode } from 'react';

import Button from 'react-bootstrap/Button';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

type TodosFooterAddButtonPropsModel = {
  children?: ReactNode;
  isTodosFormVisible: boolean;
  setIsTodosFormVisible: Dispatch<React.SetStateAction<boolean>>;
};

const TodosFooterAddButton: React.FC<TodosFooterAddButtonPropsModel> = (
  props: TodosFooterAddButtonPropsModel
) => {
  const handleTodosFooterAddButtonClick = (
    event: MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
    props.setIsTodosFormVisible(!props.isTodosFormVisible);
  };

  return (
    <Button
      className="TodosFooterAddButton--border-radius"
      onClick={handleTodosFooterAddButtonClick}
    >
      <FontAwesomeIcon icon={faPlus} />
    </Button>
  );
};

export default TodosFooterAddButton;
