import React, { Dispatch, ReactNode } from 'react';
import TodosFooterAddButton from '../../atoms/TodosFooterAddButton/TodosFooterAddButton';

type TodosFooterPropsModel = {
  children?: ReactNode;
  isTodosFormVisible: boolean;
  setIsTodosFormVisible: Dispatch<React.SetStateAction<boolean>>;
};

const TodosFooter: React.FC<TodosFooterPropsModel> = (
  props: TodosFooterPropsModel
) => {
  return (
    <div className="d-flex justify-content-center">
      <TodosFooterAddButton
        isTodosFormVisible={props.isTodosFormVisible}
        setIsTodosFormVisible={props.setIsTodosFormVisible}
      />
    </div>
  );
};

export default TodosFooter;
