import React, {
  ChangeEvent,
  Dispatch,
  MouseEvent,
  ReactNode,
  useState
} from 'react';

import ListGroupItem from 'react-bootstrap/ListGroupItem';
import { TodosItemModel } from '../../../models/TodosItemModel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPen } from '@fortawesome/free-solid-svg-icons';
import TodosForm from '../TodosForm/TodosForm';
import axios, { AxiosResponse } from 'axios';

type TodosBodyPropsModel = {
  children?: ReactNode;
  task: TodosItemModel;
  todosList: Array<TodosItemModel>;
  setTodosList: Dispatch<React.SetStateAction<Array<TodosItemModel>>>;
};

const TodosRow: React.FC<TodosBodyPropsModel> = (
  props: TodosBodyPropsModel
) => {
  const handleDeleteTaskClick = (event: MouseEvent<HTMLElement>) => {
    event.preventDefault();

    const todosList = props.todosList.slice();
    const taskIndex = todosList.findIndex(
      (e1: TodosItemModel) => e1.id === props.task.id
    );

    axios
      .delete(`http://localhost:8000/api/tasks/${props.task.id}`)
      .then((res: AxiosResponse) => {
        todosList.splice(taskIndex, 1);
        props.setTodosList(todosList);
      });
  };
  const handleEditTaskClick = (event: MouseEvent<HTMLElement>) => {
    event.preventDefault();
    setIsEdit(true);
  };
  // const handleInputClick = (event: MouseEvent<HTMLElement>) => {
  //   event.preventDefault();

  //   const todosList = props.todosList;
  //   const taskIndex = todosList.findIndex(
  //     (e1: TodosItemModel) => e1.id === props.task.id
  //   );
  //   const task = todosList[taskIndex];

  //   if (task) {
  //     task.isDone = !task.isDone;
  //     todosList[taskIndex] = task;
  //     props.setTodosList(todosList);

  //     axios
  //       .put(`http://localhost:8000/api/tasks/${props.task.id}`, props.task)
  //       .then((res: AxiosResponse) => {});
  //   }
  // };
  const handleInputClick = (event: ChangeEvent<HTMLInputElement>) => {
    // event.preventDefault();

    props.task.isDone = !props.task.isDone;
    setIsChecked(props.task.isDone);
    axios
      .put(`http://localhost:8000/api/tasks/${props.task.id}`, props.task)
      .then((res: AxiosResponse) => {});
  };
  const [isEdit, setIsEdit] = useState(false);
  const [isChecked, setIsChecked] = useState(props.task.isDone);

  if (isEdit) {
    return (
      <TodosForm
        todosList={props.todosList}
        setTodosList={props.setTodosList}
        task={props.task}
        setIsEdit={setIsEdit}
      />
    );
  } else {
    return (
      <ListGroupItem>
        <div className="d-flex justify-content-between custom-control custom-checkbox">
          <input
            type="checkbox"
            className="custom-control-input"
            id={`id-${props.task.id}`}
            checked={isChecked}
            onChange={handleInputClick}
          />
          <label
            className="custom-control-label"
            htmlFor={`id-${props.task.id}`}
          >
            {props.task.text}
          </label>
          <div className="d-flex">
            <div className="ml-3" onClick={handleEditTaskClick}>
              <FontAwesomeIcon
                className="text-muted cursor-pointer fa-fw"
                icon={faPen}
              />
            </div>
            <div className="ml-3" onClick={handleDeleteTaskClick}>
              <FontAwesomeIcon
                className="text-danger cursor-pointer fa-fw"
                icon={faTrash}
              />
            </div>
          </div>
        </div>
      </ListGroupItem>
    );
  }
};

export default TodosRow;
