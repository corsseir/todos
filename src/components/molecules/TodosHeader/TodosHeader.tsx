import './TodosHeader.css';

import React, { useState } from 'react';
import { DateTime } from 'luxon';

import TodosHeaderDayName from '../../atoms/TodosHeaderDayName/TodosHeaderDayName';
import TodosHeaderDayNumber from '../../atoms/TodosHeaderDayNumber/TodosHeaderDayNumber';
import TodosHeaderMonth from '../../atoms/TodosHeaderMonth/TodosHeaderMonth';
import TodosHeaderYear from '../../atoms/TodosHeaderYear/TodosHeaderYear';

const TodosHeader: React.FC = () => {
  const datetime = DateTime.local();

  return (
    <div className="d-flex justify-content-between align-items-center text-muted">
      <div className="d-flex">
        <TodosHeaderDayNumber datetime={datetime} />
        <div className="d-flex flex-column justify-content-center">
          <TodosHeaderMonth datetime={datetime} />
          <TodosHeaderYear datetime={datetime} />
        </div>
      </div>
      <TodosHeaderDayName datetime={datetime} />
    </div>
  );
};

export default TodosHeader;
