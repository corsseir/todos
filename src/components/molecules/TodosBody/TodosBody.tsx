import React, { Dispatch, ReactNode } from 'react';
import Alert from 'react-bootstrap/Alert';

import ListGroup from 'react-bootstrap/ListGroup';
import TodosTask from '../TodosTask/TodosTask';
import TodosForm from '../TodosForm/TodosForm';
import { TodosItemModel } from '../../../models/TodosItemModel';

type TodosBodyPropsModel = {
  children?: ReactNode;
  isTodosFormVisible: boolean;
  setIsTodosFormVisible: Dispatch<React.SetStateAction<boolean>>;
  todosList: Array<TodosItemModel>;
  setTodosList: Dispatch<React.SetStateAction<Array<TodosItemModel>>>;
};

const TodosBody: React.FC<TodosBodyPropsModel> = (
  props: TodosBodyPropsModel
) => {
  return (
    <div>
      <ListGroup className="my-3" variant="flush">
        {props.todosList.length > 0
          ? props.todosList.map((e1: TodosItemModel, i: number) => {
              return (
                <TodosTask
                  key={i}
                  task={e1}
                  todosList={props.todosList}
                  setTodosList={props.setTodosList}
                />
              );
            })
          : null}
        {props.isTodosFormVisible ? (
          <TodosForm
            isTodosFormVisible={props.isTodosFormVisible}
            setIsTodosFormVisible={props.setIsTodosFormVisible}
            todosList={props.todosList}
            setTodosList={props.setTodosList}
          />
        ) : null}
      </ListGroup>
      {props.todosList.length <= 0 && !props.isTodosFormVisible ? (
        <Alert variant="info">
          The list is empty. Try to add some tasks by clicking on button below.
        </Alert>
      ) : null}
    </div>
  );
};

export default TodosBody;
