import React, {
  ChangeEvent,
  Dispatch,
  FormEvent,
  MouseEvent,
  ReactNode,
  useState
} from 'react';

import ListGroupItem from 'react-bootstrap/ListGroupItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import { TodosItemModel } from '../../../models/TodosItemModel';
import axios, { AxiosResponse } from 'axios';

type TodosFormPropsModel = {
  children?: ReactNode;
  isTodosFormVisible?: boolean;
  setIsTodosFormVisible?: Dispatch<React.SetStateAction<boolean>>;
  task?: TodosItemModel;
  setIsEdit?: Dispatch<React.SetStateAction<boolean>>;
  todosList: Array<TodosItemModel>;
  setTodosList: Dispatch<React.SetStateAction<Array<TodosItemModel>>>;
};

const TodosForm: React.FC<TodosFormPropsModel> = (
  props: TodosFormPropsModel
) => {
  const createTask = (task: TodosItemModel) => {
    const todosList = props.todosList.slice();

    axios
      .post('http://localhost:8000/api/tasks', task)
      .then((res: AxiosResponse) => {
        task.id = res.data.id;
        todosList.push(task);
        props.setTodosList(todosList);
        props.setIsTodosFormVisible && props.setIsTodosFormVisible(false);
      });
  };
  const updateTask = (task: TodosItemModel) => {
    task.text = inputValue;
    axios
      .put(`http://localhost:8000/api/tasks/${task.id}`, task)
      .then((res: AxiosResponse) => {
        props.setIsEdit && props.setIsEdit(false);
      });
  };
  const handleFormAccept = (
    event: MouseEvent<HTMLDivElement> | FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault();

    if (inputValue.length > 0) {
      if (props.task) {
        updateTask(props.task);
      } else {
        const task: TodosItemModel = {
          isDone: false,
          text: inputValue
        };
        createTask(task);
      }

      setInputValue('');
    }
  };
  const handleCancelClick = (event: MouseEvent<HTMLDivElement>) => {
    event.preventDefault();

    if (props.task) {
      props.setIsEdit && props.setIsEdit(false);
    } else {
      props.setIsTodosFormVisible &&
        props.setIsTodosFormVisible(!props.isTodosFormVisible);
    }

    setInputValue('');
  };
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setInputValue(event.target.value);
  };
  const [inputValue, setInputValue] = useState(
    props.task ? props.task.text : ''
  );

  return (
    <ListGroupItem>
      <form className="d-flex align-items-center" onSubmit={handleFormAccept}>
        <input
          autoFocus
          className="flex-fill"
          placeholder="Enter the task..."
          type="text"
          value={inputValue}
          onChange={handleInputChange}
        />
        <div className="d-flex">
          <div className="ml-3" onClick={handleFormAccept}>
            <FontAwesomeIcon
              className="text-success cursor-pointer fa-fw"
              icon={faCheck}
            />
          </div>
          <div className="ml-3" onClick={handleCancelClick}>
            <FontAwesomeIcon
              className="text-danger cursor-pointer fa-fw"
              icon={faTimes}
            />
          </div>
        </div>
      </form>
    </ListGroupItem>
  );
};

export default TodosForm;
