import './App.css';

import React from 'react';

import TodosList from './components/organisms/TodosList/TodosList';

const App: React.FC = () => {
  return (
    <div className="container">
      <TodosList />
    </div>
  );
};

export default App;
