export type TodosItemModel = {
  id?: number;
  isDone: boolean;
  text: string;
};
