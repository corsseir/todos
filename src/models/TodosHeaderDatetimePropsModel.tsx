import { ReactNode } from 'react';
import { DateTime } from 'luxon';

export type TodosHeaderDatetimePropsModel = {
  childrend?: ReactNode;
  datetime: DateTime;
};
